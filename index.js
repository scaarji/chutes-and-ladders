'use strict';

const fs = require('fs');

const Board = require('./lib/board');
const Solver = require('./lib/solver');

const input = fs.readFileSync('./chutes_and_ladders.txt', 'utf8');
const boardDefinitions = input.split('\n\n');

let unsolvableCount = 0;
let sumMinRolls = 0;
boardDefinitions.forEach((boardDefinition) => {
  const board = Board.fromDefinition(boardDefinition);
  const minRolls = Solver.getMinRollsToVictory(board);
  if (minRolls != null) {
    sumMinRolls += minRolls;
  } else {
    unsolvableCount += 1;
  }
});
console.log('Unsolvable %s, sum of minimum number of rolls %s',
  unsolvableCount, sumMinRolls);
