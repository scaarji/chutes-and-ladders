# Chutes and Ladders

## Board

The board is a graph, in this implementation it only has size (number of vertices) and map of special edges (chutes and ladders), the rest of edges are the same type (to next 6 vertices) and storing them is redundant.

## Solver

The solver class uses Breadth-first search to go through the graph and find the minimal distance to last position of the board, if the distance is not found when all the vertices are visited, the board is considered unsolvable.

As a bonus, Solver can simulate the game with random rolls and count the number of rolls required to win, also it can calculate the average number of rolls to win using given number of trials.

## Running the task

Execute `node index.js` to run through the `chutes_and_ladders.txt` file and get the number of unsolvable boards and sum of minimal moves to win all solvable boards.

## Result

With the attached list of boards the result is:

Unsolvable 4, sum of minimum number of rolls 46363
