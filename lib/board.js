'use strict';

const TYPE_CHUTE = 'C';
const TYPE_LADDER = 'L';

class Board {
  constructor(width, height) {
    this.size = width * height;
    this.shortcuts = new Map();
  }

  setShortcut(fromPosition, toPosition) {
    this.shortcuts.set(fromPosition, toPosition);
  }

  getShortcut(fromPosition) {
    return this.shortcuts.get(fromPosition);
  }

  hasWon(position) {
    return position === this.size;
  }

  missesEnd(position) {
    return position > this.size;
  }

  static fromDefinition(text) {
    const lines = text.split('\n');
    const headerLine = lines.shift();
    const header = headerLine.split(' ');
    const width = parseInt(header[0], 10);
    const height = parseInt(header[1], 10);

    const board = new Board(width, height);
    lines.forEach((line) => {
      const chunks = line.split(' ');
      const type = chunks[0];
      const lowerPosition = parseInt(chunks[1], 10);
      const upperPosition = parseInt(chunks[2], 10);
      switch (type) {
        case TYPE_CHUTE:
          board.setShortcut(upperPosition, lowerPosition);
          break;
        case TYPE_LADDER:
          board.setShortcut(lowerPosition, upperPosition);
          break;
        default: // silently ignore invalid types
      }
    });

    return board;
  }
}

module.exports = Board;
