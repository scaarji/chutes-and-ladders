'use strict';

const DICE_SIDES = 6;

// Auxiliary class to keep track of visited positions and distances to them
class Queue {
  constructor() {
    this.distances = new Map();
    this.queue = [];
  }

  push(position, distance) {
    if (this.distances.has(position)) return; // ignore visited
    this.distances.set(position, distance);
    this.queue.push(position);
  }

  shift() {
    const currentPosition = this.queue.shift();
    const currentDistance = this.distances.get(currentPosition);
    return [currentPosition, currentDistance];
  }

  empty() {
    return this.queue.length === 0;
  }
}

class Solver {
  static getMinRollsToVictory(board) {
    const queue = new Queue();
    queue.push(0, 0);
    while (!queue.empty()) {
      const [currentPosition, currentRollsToReach] = queue.shift();

      if (board.hasWon(currentPosition)) return currentRollsToReach;

      const shortcutPosition = board.getShortcut(currentPosition);
      if (shortcutPosition != null) {
        // chute or ladder - no additional move required
        queue.push(shortcutPosition, currentRollsToReach);
      } else {
        // all possible dice rolls
        for (let roll = 1; roll <= DICE_SIDES; roll += 1) {
          const newPosition = currentPosition + roll;
          if (!board.missesEnd(newPosition)) {
            queue.push(newPosition, currentRollsToReach + 1);
          }
        }
      }
    }
    return undefined;
  }

  static getRollsToVictory(board) {
    let position = 0;
    let rollsCount = 0;
    while (!board.hasWon(position)) {
      rollsCount += 1;
      const roll = Math.floor(Math.random() * DICE_SIDES) + 1;

      if (!board.missesEnd(position + roll)) {
        // move only if have not missed the end
        position += roll;
        const shortcutPosition = board.getShortcut(position);
        if (shortcutPosition != null) {
          position = shortcutPosition;
        }
      }
    }
    return rollsCount;
  }

  static getAverageRollsToVictory(board, simulationsCount = 100000) {
    let sumRolls = 0;
    let countRuns = 0;
    for (let i = 0; i < simulationsCount; i += 1) {
      sumRolls += this.getRollsToVictory(board);
      countRuns += 1;
    }

    return sumRolls / countRuns;
  }
}

module.exports = Solver;
