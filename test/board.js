'use strict';

const fs = require('fs');

const should = require('should');

const definition = fs.readFileSync(`${__dirname}/fixture`, 'utf8');

const Board = require('../lib/board');

describe('Board', () => {
  describe('#fromDefinition', () => {
    it('should create correct board', () => {
      const board = Board.fromDefinition(definition);
      should(board).have.property('size', 100);
      should(board.getShortcut(1)).eql(38);
      should.not.exist(board.getShortcut(2));
      should(board.getShortcut(16)).eql(6);
    });
  });
});
