'use strict';

const fs = require('fs');

const should = require('should');

const definition = fs.readFileSync(`${__dirname}/fixture`, 'utf8');

const Board = require('../lib/board');
const Solver = require('../lib/solver');

describe('Solver', () => {
  describe('#getMinRollsToVictory', () => {
    it('should return correct number of min rolls', () => {
      const board = Board.fromDefinition(definition);
      should(Solver.getMinRollsToVictory(board)).eql(7);
    });

    it('should return undefined if board can not be solved', () => {
      const board = Board.fromDefinition(`7 1 6
C 0 1
C 0 2
C 0 3
C 0 4
C 0 5
C 0 6`);
      should.not.exist(Solver.getMinRollsToVictory(board));
    })
  });

  describe('#getAverageRollsToVictory', () => {
    it('should return correct number of average rolls', () => {
      const board = Board.fromDefinition(definition);
      should(Solver.getAverageRollsToVictory(board)).be.approximately(39.2, 0.2);
    });
  });
});
